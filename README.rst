========================================================
[VAGRANT][ANSIBLE] Déploiement d'un serveur PXE CentOS 7
========================================================

Ce projet permet de configurer un serveur PXE dans une VM.

OS déployés : Centos 7 + kickstart.

Requirements
------------

Nécessite l'installation de Vagrant et de Ansible.

.. code:: bash

    $ sudo apt-get update
    $ sudo apt-get install vagrant
    $ sudo apt-get install ansible
    
Nécessite l'installation de Virtualbox.

.. code:: bash

    $ echo "deb http://download.virtualbox.org/virtualbox/debian stretch contrib" >> /etc/apt/sources.list
    $ wget -q -O- https://www.virtualbox.org/download/oracle_vbox_2016.asc | apt-key add
    $ sudo apt-get update
    $ sudo apt-get install virtualbox-5.1

Déploiement
-----------
    
Clonage du repository.

.. code:: bash

    $ git clone https://gitlab.com/jordan.tan/pxe_server_kickstart && cd pxe_server_kickstart

Création de la VM avec Vagrant puis déploiement du serveur PXE avec Ansible.

.. code:: bash

    $ vagrant up
    $ #vagrant halt (stop the VM)
    $ #vagrant destroy (destroy the VM)
    
Utilisation
-----------

Il suffit de connecter les machines PXE à déployer à votre serveur PXE pour effectuer l'installation de CentOS 7.
